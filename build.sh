#!/bin/sh

export CROSS_COMPILE=riscv64-linux-gnu-
export ARCH=RISCV

# opensbi
git clone https://github.com/riscv/opensbi.git
cd opensbi
make PLATFORM=generic FW_TEXT_START=0x40000000
cd ..

# u-boot
git clone https://github.com/u-boot/u-boot.git
cd u-boot
git checkout v2023.10-rc3
git apply ../2023.10-rc3.patch

make starfive_visionfive2_defconfig
make OPENSBI=../opensbi/build/platform/generic/firmware/fw_dynamic.bin
cd ..

# Get the Starfive 7110 tools, fixup SPL with header expected by boot rom
git clone https://github.com/starfive-tech/Tools.git
cd Tools/spl_tool
make
./spl_tool -c -f ../../u-boot/spl/u-boot-spl.bin
cd ../..

cp u-boot/spl/u-boot-spl.bin.normal.out .
cp u-boot/u-boot.itb .

u-boot/tools/mkimage -A riscv -O linux -T script -C none -a 0 -e 0 -n "Distro Boot Script" -d boot.cmd boot.scr

