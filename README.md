
### Building the boot files from mainline repos

#### Step 1: Build environment

Start with empty Debian 12 "Bookworm" x86-64 as cross-compile base. Ensure the following additional packages are installed:

```
gcc
bison
flex
git
make
python3-pip
swig
device-tree-compiler
gcc-riscv64-linux-gnu
libssl-dev
bc
```

#### Step 2: Build OpenSBI

First set the `CROSS_COMPILE` and `ARCH` environment variables to the cross compiler and riscv architecture. For OpenSBI the "generic" configuration is fine, but with its start location set to 0x40000000. Building the FW_DYNAMIC target is standard for the "generic" configuration:

```
export CROSS_COMPILE=riscv64-linux-gnu-
export ARCH=RISCV

# opensbi
git clone https://github.com/riscv/opensbi.git
cd opensbi
make PLATFORM=generic FW_TEXT_START=0x40000000
cd ..
```

As part of the make many files get processed and compiled. The output looks like this:

```
 CPP-DEP   platform/generic/firmware/fw_payload.elf.dep
 CPP-DEP   platform/generic/firmware/fw_jump.elf.dep
 CPP-DEP   platform/generic/firmware/fw_dynamic.elf.dep
 CPP-DEP   platform/generic/firmware/payloads/test.elf.dep
 AS-DEP    platform/generic/firmware/fw_payload.dep
 AS-DEP    platform/generic/firmware/fw_jump.dep
 AS-DEP    platform/generic/firmware/fw_dynamic.dep
 .
 .
 AS        platform/generic/firmware/fw_payload.o
 CPP       platform/generic/firmware/fw_payload.elf.ld
 ELF       platform/generic/firmware/fw_payload.elf
 OBJCOPY   platform/generic/firmware/fw_payload.bin
```

#### Step 3: Building U-Boot

Clone mainline U-Boot repo from github and checkout the v2023.10-rc3 branch. 

This branch still has a bug (patches [here](https://patchwork.ozlabs.org/project/uboot/cover/20230818051103.2427590-1-chanho61.park@samsung.com/)). Apply the patch. Note: this patch is already in HEAD and will hence not be needed as of v2023.10-rc4 and beyond.

Building is a two step process, first building the configuration and then building the files.

Making the configuration files:

```
git clone https://github.com/u-boot/u-boot.git
cd u-boot
git checkout v2023.10-rc3
git apply ../2023.10-rc3.patch
make starfive_visionfive2_defconfig
```

This is a short process with the following output:

```
  HOSTCC  scripts/basic/fixdep
  HOSTCC  scripts/kconfig/conf.o
  YACC    scripts/kconfig/zconf.tab.c
  LEX     scripts/kconfig/zconf.lex.c
  HOSTCC  scripts/kconfig/zconf.tab.o
  HOSTLD  scripts/kconfig/conf
#
# configuration written to .config
#
```

Now build U-Boot's SPL and U-Boot proper, configured for passing the payload location and data dynamically from the SPL to OpenSBI:

```
cd u-boot
make OPENSBI=../opensbi/build/platform/generic/firmware/fw_dynamic.bin
cd ..
```

This is again a longer build and the output looks like this:

```
scripts/kconfig/conf  --syncconfig Kconfig
  UPD     include/config.h
  CFG     u-boot.cfg
  GEN     include/autoconf.mk
  .
  .
  CC      spl/common/spl/spl.o
  AR      spl/common/spl/built-in.o
  LD      spl/u-boot-spl
  OBJCOPY spl/u-boot-spl-nodtb.bin
  CAT     spl/u-boot-spl-dtb.bin
  COPY    spl/u-boot-spl.bin
  SYM     spl/u-boot-spl.sym
  MKIMAGE u-boot.img
  MKIMAGE u-boot-dtb.img
  BINMAN  .binman_stamp
  OFCHK   .config
```

The relevant output file is `u-boot.itb` in the main u-boot directory. This file is a combination of the OpenSBI binary (~250KB) and the main U-Boot binary (~750KB).

#### Step 4: Packaging SPL for the JH7110 boot rom

The JH7110 boot rom expects the SPL to have a specific header. This header can be added by a Starfive provided tool. First this tool has to be fetched and built:

```
cd ..
git clone https://github.com/starfive-tech/Tools.git
cd Tools/spl_tool
make
```
The this tool is then used to add the required header:
```
./spl_tool -c -f ../../u-boot/spl/u-boot-spl.bin
```
The tool reports the following output:

```
ubsplhdr.sofs:0x240, ubsplhdr.bofs:0x200000, ubsplhdr.vers:0x1010101 name:../../u-boot/spl/u-boot-spl.bin
SPL written to ../../u-boot/spl/u-boot-spl.bin.normal.out successfully.
```

The SPL image with header is created as `u-boot-spl.bin.normal.out`. The total length is around 140KB, well less than the 256KB that the JH7110 has available as static ram bootspace.

#### Step 5: Packaging the boot script

U-Boot requires a boot script to boot a Linux distro. A script that works with the U-Boot standard distro boot setup is:

```
# assume distro_bootcmd environment

# set the boot arguments
setenv bootargs "console=ttyS0,115200 debug rootwait earlycon=sbi root=/dev/mmcblk1p4"

# load the components and boot
fatload ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r}  vmlinuz
fatload ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r}     star64.dtb
fatload ${devtype} ${devnum}:${distro_bootpart} ${ramdisk_addr_r} initramfs.cpio.gz
booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
```

This is the file `boot.cmd`. In short it works as follows:

   - The setenv command sets the boot arguments for the linux kernel. The key part here is `root=/dev/mmcblk1p4`, the other parts are the same as default.
   - The fatload commands load the various parts of a linux boot into memory.
   - The booti command then performs a linux boot from these files.

The script variables have been set by the built-in distro_bootcmd script (defined in `include/config_distro_bootcmd.h` and `include/configs/starfive-visionfive2.h`).

This file now needs to packaged in the format that U-Boot expects:

```
u-boot/tools/mkimage -A riscv -O linux -T script \
   -C none -a 0 -e 0 -n "Distro Boot Script" -d boot.cmd boot.scr
```

The command output looks like:

```
Image Name:   Distro Boot Script
Created:      Sat Sep  2 21:39:25 2023
Image Type:   RISC-V Linux Script (uncompressed)
Data Size:    492 Bytes = 0.48 KiB = 0.00 MiB
Load Address: 00000000
Entry Point:  00000000
Contents:
   Image 0: 484 Bytes = 0.47 KiB = 0.00 MiB
```

The resulting file is `boot.scr`. This is the third and last component of the mainline boot build.

#### Step 6: Move the boot loader files to SD-Card

This step has two parts: (i) preparing the SD Card and (ii) updating the boot files.

##### preparing the SD Card

First download the last SD Card image from the fishwaldo repo to a Linux system, and write the image to a SD Card (in the example the SD Card is `/dev/sdd`, adjust for your system):

```
wget https://pine64.my-ho.st:8443/star64-image-weston-star64-2.0.wic.bz2
bzip2 -d star64-image-weston-star64-2.0.wic.bz2
dd if=star64-image-weston-star64-2.0.wic of=/dev/sdd bs=64k
```

This will create four partitions on the SD Card. The first partition is for the second stage boot loader ("SPL") and the second partition is for a combined OpenSBI + U-Boot payload. The third partition has the boot files and the fourth partition has the root file system.

Of course, when updating an existing, working card the above can be skipped. What still needs to be done, is add to add three files from step 3, 4 and 5 to the SD Card:

```
mkdir card
mount /dev/sdd4 card
cp u-boot-spl.bin.normal.out card/home/root
cp u-boot.itb card/home/root
cp boot.scr card/home/root
umount card
rmdir card
```

Now place the card in the board and boot as normal.

##### updating the boot files

Log in as "root".

The image unfortunately has the fourth partition marked with the BIOS bootable flag, and this should be the third partition. This can be corrected with the `fdisk` tool:

   - start fdisk, with the command `fdisk /dev/mmcblk1`
   - select 'x' for the extended commands
   - select 'A' to toggle the boot flag; toggle partition 4 off
   - select 'A' again, this time to toggle partition 3 on
   - select 'r' to return to the main menu
   - select 'w' to write back the partition table
   - select 'q' to leave fdisk

Next, place the various filed in the right partition, `u-boot-spl.bin.normal.out` in partition 1, `u-boot.itb` in partition 2 and add the boot script to the files in partition 3 (note that this partition is mounted as "/boot"):

```
dd if=u-boot-spl.bin.normal.out of=/dev/mmcblk1p1
dd if=u-boot.itb of=/dev/mmcblk1p2
cp boot.scr /boot/.
```

The new "standard boot" sequence of U-Boot recommends the standard Linux distro boot files to be present as separate files, not as a single "FIT" image file.
The required files can be extracted from the fishwaldo FIT image through the following commands:

```
dumpimage -T flat_dt -p 0 -l /boot/fitImage -o /boot/vmlinuz
dumpimage -T flat_dt -p 1 -l /boot/fitImage -o /boot/star64.dtb
dumpimage -T flat_dt -p 2 -l /boot/fitImage -o /boot/initramfs.cpio.gz
```

Note: it is possible to write a more elaborate boot script that extracts these files from the FIT image as part of the boot sequence (this is essentially what the script in `vf2_uEnv.txt` does) but the recommended approach seems simpler.

After this, restart the SBC and it should (re-)boot as expected:

```
shutdown -r now
```

If succesful, it is okay to remove the `fitImage` and `vf2_uEnv.txt` files from the `/boot` directory. However, it does not harm to leave them there and they would still be needed for booting from QSPI (unless that is upgraded to mainline as well).

#### Step 7: Move the boot loader files to SPI flash rom

The SPI flash can be loaded in three different ways:

   - Via an installer program that is loaded over the debug uart connection; this will work on even a totally blank system.
   - Via the u-boot command line, which requires having a working u-boot instance already installed.
   - Via the Linux command line, which requires having a working Linux system already installed.

Each context has a slightly different view of the organisation of the SPI flash rom. This is possible because the flash rom is essentially an unstructured flat space that is only divided into sections / partitions by convention. The only thing that the JH7110 internal boot rom mandates when it is set to boot from SPI is that the secondary boot loader is stored from SPI sector 0 onwards.

The installer takes the below view of SPI layout:

| Option  | Start    | Length   | Name | Content        |
|:--------|---------:|---------:|:----:|:---------------|
| "0"     |      0x0 |  0x40000 | n/a  | U-Boot SPL     |
| "2"     | 0x100000 | 0x300000 | n/a  | OpenSBI/U-Boot |

The location for the OpenSBI/U-Boot section hard coded into the installer program, which is only provided as a binary. Hence, it more or less forces U-Boot to follow this choice.

The U-Boot package itself (as configured for VF2) takes this view:

| Section | Start    | Length   | Name | Content           |
|:--------|---------:|---------:|:----:|:------------------|
| n/a     |      0x0 |  0x40000 | n/a  | U-Boot SPL        |
| n/a     |  0xf0000 |  0x10000 | n/a  | Saved environment |
| n/a     | 0x100000 | 0x300000 | n/a  | OpenSBI/U-Boot    |

As said, the SPL location is mandated by the JH7110 itself, but the other two locations are configured when U-Boot is built.

And finally, the device tree passed to Linux (again, as configured for VF2) has this view:

| Section | Start    | Length   | Name  | Content           |
|:--------|---------:|---------:|:-----:|:------------------|
| mtd0    |      0x0 |  0x40000 | spl   | U-Boot SPL        |
| mtd1    | 0x100000 | 0x300000 | uboot | OpenSBI/U-Boot    |
| mtd2    | 0xf00000 | 0x100000 | data  | General area      |

Here, too, the sections are simply configuration choices, but driven by the needs of the boot rom and the recovery program.

##### 7.1 Installing via the debug UART

Installing via the debug UART is always possible, even on a fully blank or "software bricked" system: it only requires the JH7110 itself to power up.

To install, set the JH7110 boot selector to mode 3 = "boot from uart". Connect to the uart from a host computer via an USB-to-serial cable. Then use a terminal emulation program with XMODEM facility to connect. The Quick Start Guide uses ***TerraTerm***. Other options include using ***PuTTY***, ***minicom*** or ***screen*** together with the ***lrzsz*** suite of xyzModem tools. The easiest choice is probably a recent build of ***tio*** (later than 9/2023). The examples below are with tio.

```
$ tio /dev/...
[10:59:22.829] tio v2.6
[10:59:22.830] Press ctrl-t q to quit
[10:59:22.839] Connected
```

Reset the JH7110 board. It will respond by printing a boot message followed by a stream of 'C' characters (the xmodem sync character).

First the recovery program needs to be uploaded. This program is part of the "Tools" package that was already downloaded in step 4 above. It is also available directly at https://github.com/starfive-tech/Tools/tree/master/recovery. Unfortunately, no open source version is currently available.

Press ctrl-t followed by x to start the xmodem upload.  At the prompt give the path and name of the recovery binary and press enter:

```
(C)StarFive
CCC
(C)StarFive
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
[10:59:38.749] Send file with XMODEM
[10:59:44.179] Enter file name: Tools/recovery/jh7110-recovery-20230322.bin
```

The upload will now proceed:

```
[10:59:38.749] Send file with XMODEM
[10:59:44.179] Sending file 'jh7110-recovery-20230322.bin'  
[10:59:44.179] Press any key to abort transfer
................................................................................
................................................................................
..|
[10:59:44.179] Done
```

The recovery program will automatically start and show the below menu:

```
[10:59:44.179] Done
CPU freq: 1250MHz
idcode: 0x1860C8
ddr 0x00000000, 4M test
ddr 0x00400000, 8M test
DDR clk 2133M, size 4GB

*********************************************************
****************** JH7110 program tool ******************
*********************************************************
0: update 2ndboot/SPL in flash
1: update 2ndboot/SPL in emmc
2: update fw_verif/uboot in flash
3: update fw_verif/uboot in emmc
4: update otp, caution!!!!
5: exit
NOTE: current xmodem receive buff = 0x40000000, 'load 0x********' to change.
select the function to test: 
```

Select option 0 to update the SPL binary. Repeat the steps above to send this with xmodem:

```
select the function to test: 0
send file by xmodem
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
[11:01:27.277] Send file with XMODEM
[11:01:56.405] Sending file 'u-boot-spl.bin.normal.out'  
[11:01:56.405] Press any key to abort transfer
................................................................................
........................................................|
[11:01:56.405] Done
...............
................................................................
................................................................
................................................................
................................................................
...............................updata backup section
.
................................................................
................................................................
................................................................
................................................................
................................................................
................................................................
................................................................
................................................................
...............................updata success
```

After this, the recovery program reprints the menu. Now select option 2 to update the OpenSBI/U-Boot binary. Repeat the steps above to send this with xmodem:

```
select the function to test: 2
send file by xmodem
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
[11:03:26.826] Send file with XMODEM
[11:03:30.247] Sending file 'u-boot.itb'  
[11:03:30.247] Press any key to abort transfer
................................................................................
................................................................................
<several lines skipped for brevity>
................................................................................
...............................................................................|
[11:03:30.247] Done
.
................................................................
................................................................
<several lines skipped for brevity>
................................................................
................................................................
...updata success
```

Once this completes, select option 5 to exit the recovery program.

```
select the function to test: 5

END OF SECONDBOOT
```

Now set the boot switches to 0 (= "boot from SPI") and reboot the JH7110 board to test the new install.

##### 7.2 Installing from the U-Boot command line

This method of installing requires booting from an SD Card, with working SPL and U-Boot binaries installed (see above for the steps to do this). The boot partition on the SD Card (typically partition #3) must contain copies of the SPL and U-Boot images to install to SPI. It is not necessary to have a working OS installed on this card.

*[Note: it is also possible to vary on the below and load the SPL and U-Boot images from another source, such as from a USB stick or through TFTP, or even via the uart using xmodem or ymodem transfers. Check the U-Boot documentation for details]*

Set the boot selector to booting from SD Card (value "2"), and rest the JH7110 board. As U-Boot initializes press a key to enter the U-Boot command line (instead of trying to boot an OS). First test that partition #3 contains the correct files:

```
Hit any key to stop autoboot:  0 
StarFive # fatls mmc 1:3
  9750455   vmlinuz
      556   boot.scr
  5320446   initramfs.cpio.gz
   983567   u-boot.itb
    49048   star64.dtb
   139143   u-boot-spl.bin.normal.out

6 file(s), 0 dir(s)
```

Now proceed to load the files to memory, followed by writing to the SPI flash:

```
StarFive # sf probe
SF: Detected gd25lq128 with page size 256 Bytes, erase size 4 KiB, total 16 MiB
StarFive # fatload mmc 1:3 0xa0000000 u-boot-spl.bin.normal.out
139143 bytes read in 10 ms (13.3 MiB/s)
StarFive # sf update 0xa0000000 0x0 $filesize
device 0 offset 0x0, size 0x21f87
139143 bytes written, 0 bytes skipped in 0.683s, speed 207700 B/s
StarFive # fatload mmc 1:3 0xa0000000 u-boot.itb
983567 bytes read in 45 ms (20.8 MiB/s)
StarFive # sf update 0xa000000 0x100000 $filesize
device 0 offset 0x100000, size 0xf020f
856064 bytes written, 127503 bytes skipped in 6.666s, speed 151000 B/s
StarFive # 
```

The *sf probe* command initializes the SPI communication, *fatload* loads a file into memory from a FAT formatted partition ('mmc 1:3' = mmc drive 1, partition 3). The location *0xa000000* is just a convenient location in main ram memory. The *sf update* command then loads that file into the SPI flash rom. The variable `$filesize` is set automatically by the *fatload* command that precedes it.

Set the boot switches to 0 (= "boot from SPI") and reboot the JH7110 board to test the new install.

##### 7.3 Installing from the Linux command line

This method requires a board booting to a working Linux, e.g. from an SD Card. The device tree for this Linux must include the correct section definitions for the SPI flash rom. Also, the Linux system must have the mtd-utils package installed (`apt-get install mtd-utils`).

First check the mtd partition table the Linux on the board works with:

```
$ cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00020000 00001000 "spl"
mtd1: 00300000 00001000 "uboot"
mtd2: 00100000 00001000 "data"
```

This is admittedly a bit confusing, as the starting positions of each section are not reported and it is impossible to be entirely sure that the mtd table is correct. However, if it appears correct, proceed with writing the files:

```
$ sudo flashcp -v u-boot-spl.bin.normal.out /dev/mtd0
Erasing blocks: 34/34 (100%)
Writing data: 135k/135k (100%)
Verifying data: 135k/135k (100%)
$ sudo flashcp -v u-boot.itb /dev/mtd1
Erasing blocks: 241/241 (100%)
Writing data: 960k/960k (100%)
Verifying data: 960k/960k (100%)
```

Once this completes, shutdown the system (`shutdown -h now`). Ensure that the boot switches are set to 0 (= "boot from SPI") and reboot the JH7110 board to test the new install.

