# assume distro_bootcmd environment

# set the boot arguments
setenv bootargs "console=ttyS0,115200 debug rootwait earlycon=sbi root=/dev/mmcblk1p4"

# load the components and boot
fatload ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r}  vmlinuz
fatload ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r}     star64.dtb
fatload ${devtype} ${devnum}:${distro_bootpart} ${ramdisk_addr_r} initramfs.cpio.gz
booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}

